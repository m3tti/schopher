(use ports tcp)

(define type-identifiers 
  '(("0" . TEXT) 
    ("1" . DIR)
    ("2" . CCSO)
    ("3" . ERR)
    ("4" . BIN/HEX)
    ("5" . DOS)
    ("6" . UUENCODED)
    ("7" . SRCH)
    ("8" . TEL)
    ("9" . BIN)
    ("+" . MIRROR)
    ("g" . GIF)
    ("I" . IMG)
    ("T" . TEL/3270)
    ("h" . HTML)
    ("d" . DOC)
    ("i" . INFO)
    ("s" . SND)
    ))

(define (read-gopher-response input buf)
  "recursive function to read gopher response into a list"
  (let ((line (read-line input)))
    (cond ((equal? line #!eof) buf)
          ((string= line ".") buf)
          (else (read-gopher-response input (append buf (list line)))))
    ))
 
(define (connect-to host selector port fn)
  "connects to gopher host and requests the given selector"
  "fn should be a funtion with two parameters input output"
  "fn is used to talk with the server and to modify the response"
  (define-values (i o) (tcp-connect host port))
  (write-line selector o) 
  (let ((data (fn i o))) "call function to do something with the tcp sockets"
    (close-input-port i)
    (close-output-port o)
    data))

(define (break-rest-apart line)
  "breaks response down into single strings"
  (string-split (substring line 1) "\t"))

(define (analyse-type-identifier line)
  "identifies the lines type-identifier"
  (if (string= line "")
    #f
    (let* ((type-identifier (substring line 0 1))
            (ti (assoc type-identifier type-identifiers)))
      (if ti
        (cdr ti)
        #f)
    )))

(define (parse-line line)
  "parse gopher response line"
  (let ((ti (analyse-type-identifier line)))
    (if ti
      (append (list ti) (break-rest-apart line))
      line
      )))

(define (handle-dir-response is os)
  (let ((lines (read-gopher-response is '())))
    (map parse-line lines)))

(define (handle-plain-response is os)
	(read-gopher-response is '()))

