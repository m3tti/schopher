(use srfi-13 srfi-1)

(include "gopher.scm")

(define (prop key data)
  "returns the value of a given key in a given alist"
  (cdr (assoc key data)))

(define (find-value val data)
  (when (null? data) #f)
  (if (equal? (cdar data) val)
    (caar data)
    (find-value val (cdr data))))

(define (parse-url url)
  "parses the given url and extracts host and selector"
  (let ((url-parts (string-split url "/")))
     (if (> (length url-parts) 2)
	(selective-path url-parts)
	(simple-path url-parts)
    )))

(define (simple-path url-parts)
  (list (cons 'HOST (car url-parts))
	(cons 'TYPE 'DIR)
	(cons 'SELECTOR "/")))

(define (selective-path url-parts)
  (let ((host (car url-parts))
	(type (prop (cadr url-parts) type-identifiers))
         (selector (string-append "/" (string-join (cddr url-parts) "/"))))
    (list (cons 'HOST host)
	 (cons 'TYPE type)
          (cons 'SELECTOR selector))))

(define (expand-to str size)
  "expands a string to a given size"
  (if (< (string-length str) size)
    (string-append str (make-string (- size (string-length str)) #\space))
    (string-append str "\n\t")))

(define (veronica term)
  "generates a saerch term url based on floodgaps veronica engine"
  (list (cons 'HOST "gopher.floodgap.com")
	(cons 'TYPE 'DIR)
 	(cons 'SELECTOR (string-append "/v2/vs?" term))))

(define (gopherpedia term)
  "generates a saerch term url based on floodgaps veronica engine"
  (list (cons 'HOST "gopherpedia.com")
	(cons 'TYPE 'TEXT)
 	(cons 'SELECTOR (string-append "/" term))))

(define (help)
	"Prints help screen"
	(print "schopher <option> <search term/url>")
	(print "\t-s searches veronica")
	(print "\t-g searches gopherpedia"))

(define (null-host-remove text)
  "Removes the fake or null.host in an info text"
  (cond 
    ((string=? text "null.host") "")
    ((string=? text "fake") "")
    ((string=? text "Err") "")
    (else text)))

(define (host-selector->string hs)
	"convert a host selector a list to a string"
	(string-append (prop 'HOST hs) (prop 'SELECTOR hs)))

(define (print-data data)
  "decides how to print the given data"
  (if (list? (car data))
    (map print-gopher-data data)
    (map print data)))

(define (print-gopher-data line)
  "parses the given gopher data in a nice fashion"
  (let ((ti (first line))
        (text (second line))
        (selector (third line)))
    (cond ((equal? ti 'INFO) (print (foldl string-append 
                                           "" 
                                           (list (null-host-remove text)))))
               ((equal? ti 'HTML) (print (foldl string-append 
                              "" 
                              (list  text
				  "\n\t"
				  (symbol->string ti) " [" selector "]\n"))))
	       (else (print (foldl string-append 
                              "" 
                              (list  text
				  "\n\t"
				  (symbol->string ti) " [" (fourth line) "/" (find-value ti type-identifiers)  selector "]\n"))))
                )))

(define (get-convert-fkt type)
	(cond 
		((eq? 'DIR type) handle-dir-response)
		((eq? 'FILE type) handle-plain-response)
		(else handle-plain-response)
	))

(define (connect url)
    (let ((data (connect-to 
			(prop 'HOST url) 
			(prop 'SELECTOR url) 
			70 
			(get-convert-fkt (prop 'TYPE url)))))
	(if (null? data)
		(display (string-append "No data available on " (host-selector->string url)))
      		(print-data data))
      ))
 
(define (analyse params)
 (cond 
    ((null? params) (help))
    ((string= (car params) "-h") (help))    
    ((string= (car params) "-s") (connect (veronica (last params))))    
    ((string= (car params) "-g") (connect (gopherpedia (last params))))
    (else (connect (parse-url (last params))))
  ))

(define params (cdr (argv)))
(analyse params)